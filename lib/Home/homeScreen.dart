import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math';
import '../Lobby/lobbyScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';



class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  
  SharedPreferences pref;

  bool usernameSet = false;
  bool lobbyCodeSet = false;

  final usernameController = TextEditingController();

  final lobbyCodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    checkPreferences();
  }

  void checkPreferences() async {
    pref = await SharedPreferences.getInstance();
    if (pref.getString("id") == null) {
      setNewUuidForDevice();
    }
    if (pref.getString("username") != null) {
      usernameController.text = pref.getString("username");
      this.setState(() {
        usernameSet: true;
      }
        );
    }

  }

  void setNewUuidForDevice() {
      String uuid = Uuid().v4().toString();
      pref.setString("id", uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Party Companion"),
      ),
      body: Padding(
        padding: EdgeInsets.all(50.0),
        child: Column(children: <Widget>[
          TextField(
            onChanged: (username) {
              validateUsername(username);
            },
            decoration: InputDecoration(labelText: 'Enter your username'),
            controller: usernameController,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  onChanged: (lobbyCode) {
                    validateLobbyCode(lobbyCode);
                  },
                  maxLength: 6,
                  decoration: InputDecoration(labelText: 'Lobby Code'),
                  controller: lobbyCodeController,
                  enabled: usernameSet,
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.check,
                  size: 40.0,
                ),
                onPressed:
                    (usernameSet && lobbyCodeSet) ? () => goToLobby(lobbyCodeController.text, false) : null,
              )
            ],
          ),
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: usernameSet
            ? () {
                goToLobby(generateLobbyCode(), true);
              }
            : null,
        child: Icon(Icons.add),
        tooltip: "Create Lobby",
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  validateUsername(username) {
    /*
    add whatever validation you feel like later. for now,
    i'm just gonna check if it's longer than 3 letters.
    */
    bool usernameValidated = username.length >= 3;
    if (usernameValidated) {
      pref.setString("username", username);
      print("username should be set to $username in SharedPrefs");
    }
    if (usernameValidated != usernameSet) {
      this.setState(() {
        usernameSet = usernameValidated;
      });
    }
  }

  validateLobbyCode(lobbyCode) {
    /*
    for now only checking that lobby code is 6 letters long
     */

    bool lobbyCodeValid = lobbyCode.length == 6;
    this.setState(() {
      lobbyCodeSet = lobbyCodeValid;
    });
  }



  
  goToLobby(String lobbyCode, bool host) {
    // Should this be done in hostScreen.dart?
    Firestore.instance
        .collection(lobbyCode)
        .document()
        .setData({'id': pref.getString("id"),'user': usernameController.text, 'host': host});
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LobbyScreen(
                userId: pref.getString("id"),lobbyCode: lobbyCode, username: usernameController.text)));
  }

  String generateLobbyCode() {
    /*
    TODO: Make sure the lobby code is unique.
    */
    final lobbyCodeLength = 6;
    /*
    This is probably really bad lmao. I think you can cast an int to a char with some unicode bullshit
    or something maybe do that instead
    */
    final charList = [
      'a',
      'b',
      'c',
      'd',
      'e',
      'f',
      'g',
      'h',
      'i',
      'j',
      'k',
      'l',
      'm',
      'n',
      'o',
      'p',
      'q',
      'r',
      's',
      't',
      'u',
      'v',
      'w',
      'x',
      'y',
      'z',
      '0',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9'
    ];

    String lobbyCode = "";
    Random random = new Random();
    for (int i = 0; i < lobbyCodeLength; i++) {
      final pos = random.nextInt(charList.length);
      lobbyCode += charList[pos];
    }
    return lobbyCode;
  }
}
