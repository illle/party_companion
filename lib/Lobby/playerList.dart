import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PlayerList extends StatefulWidget {
  final String lobbyCode;
  PlayerList({this.lobbyCode});

  _PlayerListState createState() => _PlayerListState();
}

class _PlayerListState extends State<PlayerList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance.collection(widget.lobbyCode).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading...");
          return ListView.builder(
            itemExtent: 80.0,
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                _buildListItem(context, snapshot.data.documents[index]),
          );
        });
  }
}

Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
  return ListTile(
    title: Row(
      children: [
        Expanded(
          child: Text(
            document['user'],
            style: Theme.of(context).textTheme.headline,
          ),
        ),
      ],
    ),
  );
}
