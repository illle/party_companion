import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'playerList.dart';
import 'chatWindow.dart';

class LobbyScreen extends StatefulWidget {
  final String lobbyCode;
  final String userId;
  final String username;
  LobbyScreen({this.userId, this.lobbyCode, this.username});

  _LobbyScreenState createState() => _LobbyScreenState();
}

class _LobbyScreenState extends State<LobbyScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = new TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.lobbyCode),
          bottom: TabBar(
            
            tabs: <Widget>[
              Tab(icon: Icon(Icons.group)),
              Tab(icon: Icon(Icons.chat)),
              Tab(icon: Icon(Icons.settings))
            ],
          ),
        ),
        body: TabBarView(
        
          children: <Widget>[
            PlayerList(lobbyCode: widget.lobbyCode),
            ChatWindow(lobbyCode: widget.lobbyCode, userId: widget.userId, username: widget.username),
            Icon(Icons.signal_cellular_null)

            ],
        )),

    );
  }
}

Widget _buildMessageItem(BuildContext context, DocumentSnapshot document) {
  return ListTile(
    title: Row(
      children: [
        Expanded(
          child: Text(
            document['message'],
            style: Theme.of(context).textTheme.headline,
          ),
        ),
      ],
    ),
  );
}
