import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class ChatWindow extends StatefulWidget {
  final String lobbyCode;
  final String userId;
  final String username;
  ChatWindow({this.lobbyCode, this.userId, this.username});

  @override
  State createState() => new ChatWindowState();
}

class ChatWindowState extends State<ChatWindow> with TickerProviderStateMixin {
  
  String userIdOfLastMessage;
  final messageController = TextEditingController();
  bool isWriting = false;

  @override
  Widget build(BuildContext ctx) {
    return Column(
      children: <Widget>[
        Expanded(
          child: StreamBuilder(
              stream: Firestore.instance
                  .collection(widget.lobbyCode + "messages")
                  .orderBy("timestamp", descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return Container();
                return ListView.builder(
                  padding: EdgeInsets.all(5.0),
                  itemExtent: 80.0,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) =>
                  
                      _buildChatMessageListItem(context, snapshot, index),
                  reverse: true,
                );
              }),
        ),
        new Divider(height: 1.0),
        new Container(
          child: _buildComposer(),
          decoration: new BoxDecoration(color: Theme.of(ctx).cardColor),
        ),
      ],
    );
  }

  sendMessage(message) {
    messageController.clear();

    Firestore.instance
        .collection(widget.lobbyCode + "messages")
        .document()
        .setData({
      "userId": widget.userId,
      "username": widget.username,
      "message": message,
      "timestamp": DateTime.now().millisecondsSinceEpoch.toString()
    });
  }

  Widget _buildChatMessageListItem(BuildContext context, AsyncSnapshot snapshot, int index) {

    DocumentSnapshot document = snapshot.data.documents[index];
    /*
    Inclusive math is too hard for me so I'm not sure why this works.
    This is for comparing the sender of the current message with the next message,
    which in this case is the message sent before, since the message are organised
    by timestamp. This is so that if the same person sends a several messages in a
    row the sender name will only be shown for the first message.
    */
    DocumentSnapshot nextDocument;
    if (snapshot.data.documents.length >= index+2) {
       nextDocument = snapshot.data.documents[index+1];
    }
    

    final String userId = document['userId'];

    /* 
    This is used to know if the message should be displayed on the right 
    side (if the current user sent the message) or on the left side (if
    someone else sent the message)
    */
    final bool youSentThis = (userId == widget.userId);

    final String username = document['username'];
    final String message = document['message'];


    Widget messageContainer = Container(
                child: new Text(message),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: youSentThis ? Colors.green : Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all()
                ),
              );

    Widget nameTextLine = new Text(username, style: Theme.of(context).textTheme.subhead);

    List<Widget> messageRow = [

    ];

    if (nextDocument != null) {
      if (nextDocument['userId'] != userId) {
        /*
        Displays name if the current message does NOT have the same
        sender as the message before
        */
        messageRow.add(nameTextLine);
      }
    } else {
      /*
      If there's no "nextDocument" it means that this is the first message
      sent, and then the name of the sender should obviously always
      be displayed.
      */

       messageRow.add(nameTextLine);
    }


    /*
    Adds the container displaying the actual message to the "message row".
    messageRow will only contain messageContainer if the nameTextLine
    wasn't added in the previous step.
    */
    messageRow.add(messageContainer);

    final returnWidget = Container(
      margin: const EdgeInsets.symmetric(vertical: 8.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
           Expanded(
          child: FractionallySizedBox(
            
            alignment: youSentThis ? FractionalOffset.centerRight : FractionalOffset.centerLeft,
        widthFactor: 0.7,
        heightFactor: 1.0,
        child: new Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: new Column(
            crossAxisAlignment:
                youSentThis ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: messageRow
          ),
        ),
      )),
        ],
      ),
    );

    userIdOfLastMessage = userId;
    return returnWidget;
  }











  // ToDo: Name this method something that isn't retarded.
  Widget _buildComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 9.0),
          child: new Row(
            children: <Widget>[
              new Flexible(
                child: new TextField(
                  controller: messageController,
                  onChanged: (String message) {
                    setState(() {
                      isWriting = message.length > 0;
                    });
                  },
                  onSubmitted: (String message) => sendMessage(message),
                  decoration: new InputDecoration.collapsed(
                      hintText: "Enter some text to send a message"),
                ),
              ),
              new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 3.0),
                  child: Theme.of(context).platform == TargetPlatform.iOS
                      ? new CupertinoButton(
                          child: new Text("Submit"),
                          onPressed: isWriting
                              ? () => sendMessage(messageController.text)
                              : null)
                      : new IconButton(
                          icon: new Icon(Icons.message),
                          onPressed: isWriting
                              ? () => sendMessage(messageController.text)
                              : null,
                        )),
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
                  border: new Border(top: new BorderSide(color: Colors.brown)))
              : null),
    );
  }
}
